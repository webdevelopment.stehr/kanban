import React, {Component} from 'react';
import './App.css';
import KanbanBoard, {KanbanCard, KanbanColumn} from './KanbanBoard'

const data = [
    {
        id: '0',
        title: "Open",
        cards: [
            {
                id: '1',
                title: "Card 1",
                content: <div>some stuff here</div>
            },
            {
                id: '2',
                title: "Card 2",
                content: <div> some stuff here </div>
            },
            {
                id: '5',
                title: "Card 2",
                content: <div> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mollis, velit id
                    gravida eleifend, erat lectus eleifend leo, varius congue magna tellus ac nibh. Nulla consequat,
                    ex sed congue porttitor, urna nibh ullamcorper lorem, nec faucibus lectus est sed leo. Phasellus
                    fringilla dolor ex, ac congue arcu aliquam vitae. Nam faucibus lacus a dui rhoncus, et auctor ex
                    consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
                    egestas. Sed ullamcorper eu orci lacinia fringilla. Etiam rhoncus orci tortor, dictum sagittis
                    velit scelerisque ac. Sed eget eros maximus, commodo lorem a, eleifend lorem. Fusce bibendum
                    diam eget diam ultricies sollicitudin. Mauris egestas eget purus vel porttitor. Aliquam ultrices
                    neque lacus, ac pharetra libero dictum at. Etiam euismod consectetur nisi et molestie. Aliquam
                    posuere tempor ligula et aliquet.

                    Nam sed porta enim. Vestibulum viverra mattis laoreet. Nullam et vestibulum tellus, eu commodo
                    mi. Praesent interdum lacus sed quam malesuada, ut tempor ante pulvinar. Curabitur convallis,
                    risus id tristique suscipit, lorem metus vehicula mi, ac scelerisque ligula nisl sit amet arcu.
                    In dictum massa ac blandit sollicitudin. Praesent scelerisque metus eu sapien auctor finibus.
                    Morbi at massa in mi elementum consectetur. Praesent sit amet orci varius, dictum eros eu,
                    fringilla enim. Curabitur quis interdum libero.

                    Vestibulum gravida pharetra leo, ac commodo turpis rutrum eu. Vestibulum sem lorem, scelerisque
                    et placerat sit amet, cursus vel urna. Sed maximus tristique enim. Phasellus malesuada tincidunt
                    enim eu gravida. Sed tincidunt tortor risus, a semper justo malesuada sit amet. Cras quam purus,
                    mollis vitae metus non, suscipit scelerisque risus. Pellentesque habitant morbi tristique
                    senectus et netus et malesuada fames ac turpis egestas. Vivamus eu metus et sem hendrerit
                    placerat. Suspendisse facilisis cursus tellus eu fermentum. Duis vehicula laoreet massa, id
                    ultrices augue lobortis vitae. Donec quis tincidunt eros.

                    Ut sapien ligula, porta eu hendrerit a, mattis nec risus. Ut a nibh tincidunt, malesuada nisi
                    ut, consequat turpis. Mauris a nulla a augue elementum varius vel at ipsum. In bibendum eros
                    vitae porttitor facilisis. Nam vitae magna eget lacus consequat porttitor vel vel purus. Etiam
                    ultricies faucibus sem id lacinia. Ut iaculis mollis nibh, et dignissim tellus volutpat nec.
                    Nullam risus justo, maximus sit amet tempor eget, condimentum vitae tortor. Duis venenatis
                    auctor luctus. In hac habitasse platea dictumst.

                    Suspendisse potenti. Mauris in leo vestibulum, ornare tortor vitae, porttitor ipsum. Sed laoreet
                    nisl vitae maximus hendrerit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices
                    posuere cubilia Curae; Sed sit amet ipsum lacus. Cras facilisis vel nisl ut porta. Phasellus leo
                    diam, dignissim non consequat et, varius vitae felis. Nunc quis mattis arcu. Nullam eu massa
                    quis lectus vehicula lacinia. Morbi eu elit malesuada, iaculis sapien dignissim, viverra massa.
                    Fusce quis massa pellentesque, varius sapien in, consectetur tellus. Praesent mi lorem, auctor
                    vulputate suscipit aliquet, maximus eget lectus. Duis scelerisque mauris eget mi maximus
                    tincidunt. Maecenas nec feugiat mauris, vel tincidunt erat. Nulla hendrerit porta nunc, nec
                    sodales mauris ullamcorper ac. Phasellus et arcu vitae sem laoreet elementum ut ac sem. </div>
            },
            {
                id: '6',
                title: "Card 2",
                content: <div> some stuff here </div>
            },
            {
                id: '7',
                title: "Card 2",
                content: <div> some stuff here </div>
            },
            {
                id: '8',
                title: "Card 2",
                content: <div> some stuff here </div>
            },
            {
                id: '9',
                title: "Card 2",
                content: <div> some stuff here </div>
            },
            {
                id: '10',
                title: "Card 2",
                content: <div> some stuff here </div>
            }
        ],
        color: 'blue',
        isDefaultColumn: true,
        isPrimaryColumn: true
    },
    {
        id: '1',
        title: "In Progress",
        cards: [
            {
                id: '3',
                title: "Card 3",
                content: <div> progress </div>
            }
        ],
        color: 'red',
        isDefaultColumn: true,
        isPrimaryColumn: false
    },
    {
        id: '2',
        title: "Published",
        cards: [
            {
                id: '4',
                title: "Card 4",
                publishDate: '1995-12-17T03:24:00',
                content: <div> Im finished </div>
            }
        ],
        color: 'green',
        isDefaultColumn: true,
        isPrimaryColumn: false
    }
];

class App extends Component {

    updateColumn = (column:KanbanColumn) => {
        console.info("update column", column)
    };

    addColumn = (column:KanbanColumn, columnIndex:number) => {
        console.info("add column - column: ", column, "at index: ", columnIndex)
    };

    removeColumn = (column:KanbanColumn) => {
        console.info("remove column - column: ", column)
    };

    render () {
        return (
            <div className="App">
                <KanbanBoard
                    data={data}
                    updateColumn={this.updateColumn}
                    addColumn={this.addColumn}
                    removeColumn={this.removeColumn}
                />
            </div>
        )
    }
}

export default App;
