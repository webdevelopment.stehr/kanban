import React, {Component, createRef} from 'react'
import interact from 'interactjs'
import {KanbanCard, KanbanColumn} from "../KanbanBoard";
import {InteractEvent} from "@interactjs/types/types";
import KanbanCardComponent from "./KanbanCardComponent";

interface Props {
    column:KanbanColumn
    sourceColumnId?:string
    insertCardAtIndex:(event:InteractEvent, destinationColumn:string, index:number) => void
    setDraggedCard:(card:KanbanCard, draggedFromCol:string) => void
    //setIndex: (index:number) => void
    showDetails:boolean
}

interface State {

}

class KanbanCardColumn extends Component<Props, State> {

    private zoneRef = createRef<HTMLDivElement>();

    constructor(props:Props) {
        super(props);

        this.state = {

        }
    }

    resetColumnStyling = () => {
        //reset the bg color styling
        const node: HTMLDivElement|null = this.zoneRef.current;
        if(node) {
            node.style.background = this.props.column.color
        }
    };

    componentDidMount(){
        if(this.zoneRef.current !== null) {
            const node: HTMLDivElement = this.zoneRef.current;
            interact(node)
                .dropzone({
                    accept:'.draggableCard',
                    overlap: 1,
                    ondragenter: this.onDragEnter,
                    ondragleave: this.onDragLeave,
                    ondrop: this.onDrop
                });

            node.style.background = this.props.column.color
        }
    }

    onDrop = (event:InteractEvent) => {
        let index:number;

        if(this.props.column.id === this.props.sourceColumnId) {
            //same column
            index = this.props.column.cards.length -1
        } else {
            index = this.props.column.cards.length
        }

        this.props.insertCardAtIndex(event, this.props.column.id,index);

        this.resetColumnStyling()
    };

    onDragEnter = (event:any) => {
        //mark the dragged over column with bg color
        const node: HTMLDivElement|null = this.zoneRef.current;
        if(node) {
            node.style.background = 'LightSlateGray'
        }
    };

    onDragLeave = (event:any) => {
        this.resetColumnStyling()
    };

    render () {
        const {column, sourceColumnId} = this.props;

        return (
            <div className='dropZone' id={column.id} style={{ backgroundColor:column.color}} ref={this.zoneRef}>
                {
                    column.cards.map((card) =>
                        <KanbanCardComponent
                            key={card.id}
                            card={card}
                            columnId={column.id}
                            sourceColumnId={sourceColumnId}
                            insertCardAtIndex={this.props.insertCardAtIndex}
                            showDetails={this.props.showDetails}
                            setDraggedCard={this.props.setDraggedCard}
                            resetColumnStyling={this.resetColumnStyling}
                            //setIndex={this.props.setIndex}
                        />
                    )
                }
            </div>
        )
    }
}

export default KanbanCardColumn