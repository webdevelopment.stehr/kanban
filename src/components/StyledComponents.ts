import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button/Button";
import Input from "@material-ui/core/Input/Input";
import FormHelperText from "@material-ui/core/FormHelperText/FormHelperText";

export const ColumnButton = withStyles(() => ({
    root: {
        color: 'white',
        '&:hover': {backgroundColor: 'rgba(255, 255, 255, 0.5)'},
        float: 'right',
    },
}))(Button);

export const ShowDetailsButton = withStyles(() => ({
    root: {
        marginBottom: '20px',
    },
}))(Button);

export const SmallInput = withStyles(() => ({
    root: {
        float:'right',
        color: 'white',
    },
    underline: {
        borderBottom: '2px solid white',
        '&:after': {
            borderBottom: '2px solid white'
        },
        '&:before': {
            borderBottom: 'none'
        },
        '&:hover&:before': {
            borderBottom: '2px solid white'
        },
        '&:hover': {
            borderBottom: '2px solid white'
        }
    }
}))(Input);

export const CustomFormHelper = withStyles(() => ({
    root: {
        color:'white'
    }
}))(FormHelperText);