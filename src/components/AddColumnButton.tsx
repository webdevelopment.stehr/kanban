import React, {Component} from 'react'
import {ColumnButton, CustomFormHelper, SmallInput} from "./StyledComponents";
import FormControl from "@material-ui/core/FormControl/FormControl";

interface State {
    showAddButton:boolean
    columnTitle?:string
    titleInputError:string
}

interface Props {
    index:number
    onConfirm: (index: number, title:string) => void
}

class AddColumnButton extends Component<Props,State> {

    state:State = {
        showAddButton:true,
        columnTitle:'',
        titleInputError:''

    };

    onButtonClick = () => {
        this.setState({showAddButton:!this.state.showAddButton})
    };


    textFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            columnTitle:event.target.value
        });
    };

    resetState = () => this.setState({
        showAddButton:true,
        columnTitle:'',
        titleInputError:''
    });

    confirmWithKeyStroke = (event:React.KeyboardEvent) => {
        if(event.key === 'Enter' && this.state.columnTitle !== undefined) {
            if(this.state.columnTitle.length < 1) {
                this.setState({
                    titleInputError:'enter at least one character'
                })
            } else {
                this.props.onConfirm(this.props.index, this.state.columnTitle);

                this.resetState()
            }
        }
    };

    render () {
        const {showAddButton, columnTitle, titleInputError} = this.state;
        return (
            <div>
                {
                    showAddButton ?
                        <ColumnButton size='small' onClick={this.onButtonClick} >add column ></ColumnButton> :
                        <div>
                            <ColumnButton size='small' onClick={this.resetState}>x</ColumnButton>
                            <FormControl style={{float:'right'}}>
                                <SmallInput
                                    value={columnTitle}
                                    onChange={this.textFieldChange}
                                    onKeyPress={this.confirmWithKeyStroke}
                                    autoFocus={true}
                                />
                                <CustomFormHelper>{titleInputError ? titleInputError : 'please enter a title'}</CustomFormHelper>
                            </FormControl>
                        </div>
                }
            </div>
        )
    }
}

export default AddColumnButton