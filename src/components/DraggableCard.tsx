import React, {Component, createRef, RefObject} from 'react'

import interact from 'interactjs'

import {Card} from "@material-ui/core";
import moment from "moment";
import {KanbanCard} from "../KanbanBoard";
import CardContent from "@material-ui/core/CardContent/CardContent";
import {InteractEvent} from "@interactjs/types/types";

const toLocalDate = (dateString:string) => {
    return moment(dateString).format('LL');
};

interface Props {
    card:KanbanCard,
    showDetails:boolean,
    columnId:string,
    setDraggedCard:(card:KanbanCard, draggedFromCol:string) => void
}

class DraggableCard extends Component<Props> {

    private draggableCardRef:RefObject<HTMLDivElement> = createRef<HTMLDivElement>();

    componentDidMount() {
        const draggableCard = this.draggableCardRef.current;
        if(draggableCard) {
            interact(draggableCard)
                .draggable({
                    autoScroll: true,
                    onmove: this.onDrag,
                    onend: this.onEnd,
                    onstart: this.onStart,
                    snap:{
                        targets: [],
                        endOnly:true
                    }
                })
        }
    }

    onStart = (event:InteractEvent) => {
        const {columnId, card} = this.props;
        this.props.setDraggedCard(card, columnId)
    };

    onEnd = (event:InteractEvent) => {
        //..reset dragged cards delta transforms and appended styles
        const thisCard = this.draggableCardRef.current;
        if(thisCard) {
            thisCard.removeAttribute('data-x');
            thisCard.removeAttribute('data-y');
            thisCard.removeAttribute('style');
        }
    };

    onDrag = (event:InteractEvent) => {
        let node = this.draggableCardRef.current;

        if(node !== null) {

            const dataX = parseFloat(node.getAttribute('data-x') || '0');
            const dataY = parseFloat(node.getAttribute('data-y') || '0');

            let x:number,y:number;

            x = dataX + event.dx;
            y = dataY + event.dy;

            node.style.transform = 'translate(' + x + 'px, ' + y + 'px)';

            node.setAttribute('data-x', x.toString());
            node.setAttribute('data-y', y.toString());
        }
    };

    render() {
        const {card, showDetails} = this.props;

        return (
            <Card className='draggableCard' ref={this.draggableCardRef} key={card.id} raised>
                <div className='cardId'>id:{card.id}</div>
                <h3>{card.title}</h3>
                <div className='cardDate'>Publish Date: {card.publishDate ? toLocalDate(card.publishDate) : ''}</div>
                <CardContent className='cardContent'>
                    {showDetails ? card.content : ''}
                </CardContent>
            </Card>
        )
    }


}

export default DraggableCard