import React, {Component} from 'react'
import Button from "@material-ui/core/Button/Button";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DeleteIcon from '@material-ui/icons/Delete';
import {KanbanColumn} from "../KanbanBoard";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";

interface State {
    open:boolean
    moveCards:boolean
}

interface Props {
    column:KanbanColumn
    removeColumn: (column:KanbanColumn, moveCards:boolean) => void
}

class DeleteColumnDialog extends Component<Props,State> {

    state:State = {
        open:false,
        moveCards:false
    };

    openDialog = () => {
        this.setState({open:true})
    };

    closeDialog = () => {
        this.setState({open:false})
    };

    removeColumnConfirm = () => {
        this.props.removeColumn(this.props.column,this.state.moveCards);
        this.closeDialog();
    };

    deleteCardsToggle = () => {
        this.setState({moveCards: !this.state.moveCards})
    };

    render () {
        return (
            <div>
                <Button className='deleteColumnButton' variant='contained' size='small' color='primary'
                        onClick={this.openDialog}>
                    Delete
                    <DeleteIcon />
                </Button>
                <Dialog open={this.state.open} onClose={this.closeDialog}>
                    <DialogTitle>Delete State <span style={{fontWeight:'bold'}}>{this.props.column.title}</span> ?</DialogTitle>
                    <DialogContent>
                        <p>All cards ({this.props.column.cards.length}) in the State <span style={{fontWeight:'bold'}}>{this.props.column.title}</span> you are about to remove, will be lost!</p>
                        Or move all Cards to Open State?<Checkbox checked={this.state.moveCards} onChange={this.deleteCardsToggle} />
                    </DialogContent>
                    <DialogActions>
                        <Button color='primary' onClick={this.closeDialog}>no</Button>
                        <Button color='secondary' onClick={this.removeColumnConfirm}>yes</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default DeleteColumnDialog