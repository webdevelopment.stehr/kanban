import React, {Component, createRef, RefObject} from 'react'
import interact from "interactjs";
import {InteractEvent} from "@interactjs/types/types";
import {KanbanCard} from "../KanbanBoard";
import DraggableCard from "./DraggableCard";

interface Props {
    card: KanbanCard
    columnId: string
    sourceColumnId?: string
    insertCardAtIndex: (event: InteractEvent, destinationColumn: string, index:number) => void
    showDetails: boolean
    setDraggedCard:(card:KanbanCard, draggedFromCol:string) => void
    resetColumnStyling:() => void
    //setIndex: (index:number) => void
}

class KanbanCardComponent extends Component<Props> {

    private cardDropZoneRef:RefObject<HTMLDivElement> = createRef<HTMLDivElement>();

    resetStyling = () => {
        const cardDZRef = this.cardDropZoneRef.current;
        if(cardDZRef) {
            cardDZRef.classList.remove('dropTargetStyleCard');
            cardDZRef.style.borderTop = '';
            cardDZRef.style.borderBottom = '';
        }
        this.props.resetColumnStyling()
    };

    setBorderStyling = (below:boolean) => {
        const thisCardDropZone = this.cardDropZoneRef.current;
        if(thisCardDropZone) {
            if(below) {
                thisCardDropZone.style.borderTop = '';
                thisCardDropZone.style.borderBottom = '2px solid red';
            } else {
                thisCardDropZone.style.borderBottom = '';
                thisCardDropZone.style.borderTop = '2px solid red';
            }
        }
    };

    componentDidMount(){
        const cardDZRef = this.cardDropZoneRef.current;
        if(cardDZRef) {
            interact(cardDZRef)
                .dropzone({
                    accept:'.draggableCard',
                    overlap: 0.5,
                    ondragenter: this.onEnterCardDz,
                    ondragleave: this.onLeaveCardDz,
                    ondrop: this.onDropOnCardDz,
                    ondropmove: this.onMovingCardOverCardDz,
                });
        }
    }

    distanceToTop = (element:Element) => element.getBoundingClientRect().top;
    checkIfCardIsBelowCard = (elementA:Element, elementB:Element) => (this.distanceToTop(elementA) - this.distanceToTop(elementB))>0;


    //TODO check if this causes trouble
    private insertIndex?:number;

    onMovingCardOverCardDz = (event:InteractEvent) => {
        const thisCardDropZone = this.cardDropZoneRef.current;
        const draggedCard = event.relatedTarget;

        if(thisCardDropZone){
            //check if moving card over it self
            if(thisCardDropZone.firstElementChild !== draggedCard) {
                let index:number|undefined;
                if(thisCardDropZone.parentElement) {
                    const neighborsOfThisDZ = thisCardDropZone.parentElement.children;
                    index = Array.prototype.indexOf.call(neighborsOfThisDZ, thisCardDropZone);
                    //check if in same column to adjust the index
                    if(this.props.columnId === this.props.sourceColumnId) {
                        //same column
                        const draggedCardIndex = Array.prototype.indexOf.call(neighborsOfThisDZ, draggedCard.parentElement);
                        if(index > draggedCardIndex) {
                            index--;
                        }
                    }
                    //check if moving dragged card above or below current dz card too adjust index
                    const thisCardDropZoneCardElement = thisCardDropZone.firstElementChild;
                    if(thisCardDropZoneCardElement) {
                        if(this.checkIfCardIsBelowCard(draggedCard, thisCardDropZoneCardElement)) {
                            //place card below
                            index++;
                            this.setBorderStyling(true);
                        } else {
                            //place card above
                            this.setBorderStyling(false);
                        }
                    }
                }
                if(index !== undefined)
                    this.insertIndex = index;
            } else {
                //moving card over it self -> do nothing
                this.insertIndex = undefined
            }
        }
    };

    onDropOnCardDz = (event:InteractEvent) => {

        if(event.currentTarget === event.relatedTarget.parentElement) {
            //drop card over it self -> don't update
        } else {
            if(this.insertIndex !== undefined)
                this.props.insertCardAtIndex(event, this.props.columnId, this.insertIndex);
        }

        //rest dragged card
        event.relatedTarget.removeAttribute('data-x');
        event.relatedTarget.removeAttribute('data-y');
        event.relatedTarget.removeAttribute('style');

        this.resetStyling();
    };

    onEnterCardDz = (event:InteractEvent) => {
        // mark drop target with bg color
        event.currentTarget.classList.add('dropTargetStyleCard');
        // mark the column this card drop target is in (its parent)
        if(event.currentTarget.parentElement)
            event.currentTarget.parentElement.style.background = 'lightgrey';
    };

    onLeaveCardDz = (event:InteractEvent) => {
        this.resetStyling();
    };

    render () {
        const {card, showDetails, columnId} = this.props;
        return (
            <div className='cardDropZone' ref={this.cardDropZoneRef}>
                <DraggableCard
                    card={card}
                    showDetails={showDetails ? showDetails : false}
                    columnId={columnId}
                    setDraggedCard={this.props.setDraggedCard}/>
            </div>
        )
    }
}

export default KanbanCardComponent