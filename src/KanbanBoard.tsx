import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import uniqid from "uniqid"

import KanbanCardColumn from "./components/KanbanCardColumn";
import DeleteColumnDialog from "./components/deleteColumnDialog";
import AddColumnButton from "./components/AddColumnButton";
import {ShowDetailsButton} from "./components/StyledComponents";
import {InteractEvent} from "@interactjs/types/types";

export interface KanbanCard {
    id:string
    title:string
    publishDate?:string
    content:object
}

export interface KanbanColumn {
    id:string
    title:string
    cards:Array<KanbanCard>
    color:string
    isDefaultColumn:boolean
    isPrimaryColumn:boolean
}

interface State {
    columns:Array<KanbanColumn>
    showDetails?:boolean
    draggedCard?:KanbanCard
    draggedCardSourceColumn?:string
    //insertIndex?:number
}

interface Props {
    data: Array<KanbanColumn>
    updateColumn: (column:KanbanColumn) => void
    addColumn: (column: KanbanColumn, columnIndex:number) => void
    removeColumn: (column: KanbanColumn) => void
}

class KanbanBoard extends Component<Props,State> {

    constructor(props:Props) {
        super(props);

        this.state = {
            columns:this.props.data,
            showDetails:false
        }
    }

    // is called on start dragging a card to track which card is currently dragged and its source column
    setDraggedCard = (card:KanbanCard, draggedFromCol:string) => this.setState({draggedCard: card, draggedCardSourceColumn:draggedFromCol});
    //setIndex = (index:number) => this.setState({insertIndex:index});

    insertCardAtIndex = (event:InteractEvent, destinationColumn:string, index:number) => {
        const {draggedCard, draggedCardSourceColumn} = this.state;
        if(draggedCard !== undefined && draggedCardSourceColumn !== undefined) {
            const sourceColumn = this.state.columns.find(column => column.id === draggedCardSourceColumn);
            if(sourceColumn !== undefined) {
                const droppedCardIndex = sourceColumn.cards.indexOf(draggedCard);
                //dropped on same column
                if(draggedCardSourceColumn === destinationColumn) {
                    //dropped on same column but different index -> reorder column
                    if(droppedCardIndex !== index) {
                        const reorderedCards = [...sourceColumn.cards];

                        const [splicedCard] = reorderedCards.splice(droppedCardIndex,1);

                        reorderedCards.splice(index, 0, splicedCard);

                        //make a copy for the update function
                        let columnCopy:KanbanColumn = {...this.state.columns.find(column => column.id === draggedCardSourceColumn)} as KanbanColumn;
                        columnCopy.cards = reorderedCards;

                        this.setState(prevState => ({
                            columns: prevState.columns.map(column => {
                                if(column.id === draggedCardSourceColumn)
                                    return {
                                        ...column,
                                        cards: reorderedCards
                                    };

                                return column;
                            }),
                            draggedCard:undefined,
                            draggedCardSourceColumn:undefined
                        }));

                        this.props.updateColumn(columnCopy)

                    } else {
                        //dropped on same column at the same position -> just reset currently dragged card and its source
                        this.setState({draggedCard:undefined, draggedCardSourceColumn:undefined})
                    }
                } else {
                    //dropped on different column -> move card to new column
                    //find and copy the dragged card
                    const card = {...sourceColumn.cards.find(card => card.id === draggedCard.id)} as KanbanCard;

                    if(card !== undefined) {
                        //making copies for update function
                        let columnRemovedCards:KanbanColumn = {...this.state.columns.find(col => col.id === draggedCardSourceColumn)} as KanbanColumn;
                        columnRemovedCards.cards = columnRemovedCards.cards.filter(card => card.id !== draggedCard.id);

                        let columnAddedCards:KanbanColumn = {...this.state.columns.find(col => col.id === destinationColumn)} as KanbanColumn;
                        columnAddedCards.cards = [...columnAddedCards.cards].splice(index, 0, card);

                        this.setState(prevState => ({
                            columns: prevState.columns.map(column => {
                                //remove dragged card from prev column
                                if(column.id === draggedCardSourceColumn)
                                    return {
                                        ...column,
                                        cards: column.cards.filter(card => card.id !== draggedCard.id)
                                    };

                                //ignore unaffected columns
                                if(column.id !== destinationColumn)
                                    return column;

                                //add dropped card to new column
                                let cardsToUpdate = [...column.cards];
                                cardsToUpdate.splice(index, 0, card);

                                return{
                                    ...column,
                                    cards: cardsToUpdate
                                }
                            }),
                            draggedCard:undefined,
                            draggedCardSourceColumn:undefined
                        }));

                        this.props.updateColumn(columnRemovedCards);
                        this.props.updateColumn(columnAddedCards);
                    }
                }
            }
        }
    };

    addColumn = (index:number, title:string) => {
        const {columns} = this.state;

        let columnsCopy = [...columns];
        const newColumn:KanbanColumn = {
            id:uniqid(), //using uniqid to generate a unique id for new columns, in production the id could come from a backend
            title: title,
            cards:[],
            color:'purple',
            isDefaultColumn:false,
            isPrimaryColumn:false
        };
        //index+1 since new columns added to the right, for now..
        const indexWithOffset = index+1;

        columnsCopy.splice(indexWithOffset,0, newColumn);

        this.setState({
            columns:columnsCopy
        });

        this.props.addColumn(newColumn,indexWithOffset);
    };

    removeColumn = (columnToRemove:KanbanColumn, moveCards:boolean) => {
        if(!columnToRemove.isDefaultColumn) {

            if(moveCards) {
                //save the cards from the column which is about to be removed
                const cardsToMove = [...columnToRemove.cards];

                //making a copy to pass to the update function
                let primaryColumnCopy:KanbanColumn = {...this.state.columns.find((col:KanbanColumn) => col.isPrimaryColumn)} as KanbanColumn;
                primaryColumnCopy.cards = primaryColumnCopy.cards.concat(cardsToMove);

                // add the removed cards to primary column (stage open)
                this.setState(prevState => ({
                    columns: prevState.columns.map(column => {
                        if(column.isPrimaryColumn) {
                            return {
                                ...column,
                                cards: primaryColumnCopy.cards //column.cards.concat(cardsToMove)
                            };
                        }
                        return column
                    })
                }));

                this.props.updateColumn(primaryColumnCopy)
            }

            // remove the selected column
            this.setState(prevState => ({
                columns: prevState.columns.filter(column => column.id !== columnToRemove.id)
            }));

            this.props.removeColumn(columnToRemove)
        }
    };

    onToggleDetails = () => {
        this.setState({showDetails:!this.state.showDetails})
    };

    kanbanHeader = () => {
        const {columns} = this.state;
        return (
            <div className='kanbanHeader'>
                <h1>Kanban</h1>
                {columns.length < 6 ? <p className='comment'>Demo</p> : <p className='comment'>enough columns for now until horizontal scrolling gets implemented</p>}
                <div><ShowDetailsButton color="primary" variant='contained' onClick={() => this.onToggleDetails()} >show Details</ShowDetailsButton></div>
            </div>
        )
    };

    columnHeader = (column:KanbanColumn, index:number) => {
        const {columns} = this.state;
        return (
            <div className='kanbanColumnHeader' style={{backgroundColor:column.color}}>
                <div className='buttonBar'>
                    { columns.length < 6 ? <AddColumnButton index={index} onConfirm={this.addColumn}/> : '' }
                    {!column.isDefaultColumn ? <DeleteColumnDialog column={column} removeColumn={this.removeColumn}/> : ''}
                </div>
                <h2 className='columnTitle'>{column.title}</h2>
            </div>
        )
    };

    render(){
        const { columns, showDetails, draggedCardSourceColumn } = this.state;

        return (
            <div className='kanbanBoard'>
                {this.kanbanHeader()}
                <Grid container>
                    {
                        columns.map((column, index) => (
                            <Grid item xs
                                  key={column.id}>
                                {this.columnHeader(column,index)}
                                <KanbanCardColumn key={column.id}
                                                  column={column}
                                                  sourceColumnId={draggedCardSourceColumn}
                                                  insertCardAtIndex={this.insertCardAtIndex}
                                                  showDetails={showDetails ? showDetails : false}
                                                  setDraggedCard={this.setDraggedCard}
                                                  //setIndex={this.setIndex}
                                />
                            </Grid>
                        ))
                    }
                </Grid>

            </div>
        )
    }
}

export default KanbanBoard;